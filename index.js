const path = require("path");
const express = require("express");
const cors = require('cors');
const SSEServer = require("./mySSEServer");
const Discord = require("discord.js");
const { token } = require("./token.json");
if (token === "YOUR TOKEN HERE") {
    console.error("please set your token in token.json!")
    return;
}

const mySSEServ = new SSEServer();
const discordClient = new Discord.Client({messageCacheMaxSize: 10000});

const app = express();
app.use(express.static('../webui/build'))
app.use(cors());

const formatDate = (date = new Date()) => `${date.getDate()} ${date.getMonth() + 1} ${date.getFullYear()}`

async function fetchUpTo(date, channel) {
    if (channel.messages.cache.size == 0)
        await channel.messages.fetch();

    var oldestMessage = getOldest(channel.messages.cache, (msg => msg.createdTimestamp));
    while (oldestMessage.createdAt > date) {
        const msgs = await channel.messages.fetch({before: oldestMessage.id})
        oldestMessage = getOldest(msgs, (msg => msg.createdTimestamp))
    }
}

function getOldest(reduceable, getDate = e => e, compare = (a, b) => a < b, startDateValue = Infinity) {
    return reduceable.reduce((prevState, currItem) => {
        const currDate = getDate(currItem);
        return compare(currDate, prevState.date) ? {date: currDate, item: currItem} : prevState
    }, {date: startDateValue, item: null}).item;
}

app.get('/initData', (req, res) => {
    const date = new Date(req.query.d);
    const serverId = req.query.s;

    const guild = discordClient.guilds.cache.get(serverId);
    if (!guild) {
        res.json({botIsInGuild: false});
        return;
    }
    if (!guild.available) {
        res.json({serverAvailable: false});
        return;
    }
    const [tchans, ochans] = guild.channels.cache.partition(chan => chan.type === "text");
    const vchans = ochans.filter(chan => chan.type === "voice");
    const members = guild.members.cache;

    const ret = {
        botIsInGuild: true,
        serverAvailable: guild.available,
        tchans: tchans
            .filter(chan => chan.viewable)
            .map(chan => ({id: chan.id, name: chan.name, pos: chan.rawPosition, serverId: chan.guild.id})),
        vchans: vchans
            .filter(chan => chan.viewable)
            .map(chan => ({id: chan.id, name: chan.name, pos: chan.rawPosition, serverId: chan.guild.id})),
        users: members.map(member => ({
            id: member.id,
            name: member.displayName,
            avatarURL: member.user.avatarURL() || `https://cdn.discordapp.com/embed/avatars/${member.user.discriminator % 5}.png`,
            VcState: cleanObjFromGuildVcUpdate(member.voice).state
        }))
    };

    res.json(ret);
})

app.get('/amsgs/', async (req, res) => {
    const date = new Date(req.query.d);
    date.setUTCHours(0, 0, 0);
    const dateDayEnd = new Date(date);
    dateDayEnd.setUTCHours(23, 59, 59);
    const channelId = req.query.c;

    console.log("/amsg", date, channelId);
    if (!channelId || !date) {
        //TODO specific error for date
        res.json({channelAvailable: false});
        return;
    }

    const channel = discordClient.channels.cache.get(channelId);
    if (!channel) {
        res.json({channelAvailable: false});
        return;
    }
    await fetchUpTo(date, channel);

    const msgs = channel.messages.cache.array().filter(msg => msg.createdAt > date && msg.createdAt < dateDayEnd);
    const cleaned = msgs.map(cleanObjFromGuildTxtMsg)

    res.json({
        channelAvailable: true,
        messages: cleaned
    });
})

discordClient.on('message', (msg) => {
    console.log("discord message! sending via SSE", cleanObjFromGuildTxtMsg(msg))
    mySSEServ.sendToAll(null, "dmessage", cleanObjFromGuildTxtMsg(msg))
})

discordClient.on('voiceStateUpdate', (oldState, newState) => {
    mySSEServ.sendToAll(null, "voiceupdate", cleanObjFromGuildVcUpdate(newState))
})

app.get('/stream', (req, res) => {
    console.log("adding client");
    mySSEServ.addClient(req, res);
})

app.get('*', (req, res) => {
    const filePath = path.resolve(__dirname, "../webui/build/index.html");
    res.sendFile(filePath);
})

app.listen(2204);
discordClient.login(token);
discordClient.on("ready", (data) => console.log("connected!", data));


const cleanObjFromGuildTxtMsg =(msg) => ({
    author: msg.author.id,
    content: msg.content,
    chan: msg.channel.id,
    server: msg.channel.guild.id,
    id: msg.id,
    date: msg.createdAt.toISOString()
})

const cleanObjFromGuildVcUpdate = (state) => ({
    userId:     state.id,
    state: {
        serverDeaf: state.serverDeaf,
        serverMute: state.serverMute,

        selfDeaf:   state.selfDeaf,
        selfMute:   state.selfMute,

        //sessionID:  state.sessionID,
        //streaming:  state.streaming,

        serverId:   state.guild.id,
        channelId:  state.channelID
    }
})

setInterval(() => mySSEServ.sendToAll(null, "ping", {date: new Date().toISOString()}), 1000 * 60);
